
package com.nuebus.servidorimpresion.versionado;

import com.nuebus.servidorimpresion.utiles.Utiles;

/**
 *
 * @author Valeria
 */
public class VersionProducto {
    private static final int MAJOR = 2; 
    private static final int MINOR = 0;
    private static final String versionJava= "1.8.0_45 o superior" ; 
    
    public static String getVersion(){
        return String.valueOf(MAJOR) + "." + String.valueOf(MINOR);
    }
    
    public static String getVersionJava(){
        return versionJava;
    }
    
    public static String getAnioCopyRigth(){
        return "2001 - " + Utiles.parseDateToString(new java.util.Date(), Utiles.FORMAT_YEAR);
    }
}
