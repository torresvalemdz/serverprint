package com.nuebus.servidorimpresion.presentacion.helpers.impl;

import com.nuebus.servidorimpresion.negocio.ColaImpresion.MessageBoleto;
import com.nuebus.servidorimpresion.presentacion.BuilderElemento.Elemento;
import com.nuebus.servidorimpresion.presentacion.BuilderElemento.TipoElementoEnum;
import com.nuebus.servidorimpresion.negocio.impresion.PageCanva;
import com.nuebus.servidorimpresion.entities.ImpresoraFisica;
import com.nuebus.servidorimpresion.presentacion.BuilderElemento.ElementoCodigoBarraFactory;
import com.nuebus.servidorimpresion.presentacion.BuilderElemento.ElementoCodigoPDFFactory;
import com.nuebus.servidorimpresion.presentacion.BuilderElemento.ElementoFactory;
import com.nuebus.servidorimpresion.presentacion.BuilderElemento.ElementoImagenFactory;
import com.nuebus.servidorimpresion.presentacion.BuilderElemento.ElementoTextoFactory;
import com.nuebus.servidorimpresion.presentacion.services.IImpresoraService;
import com.nuebus.servidorimpresion.utiles.Utiles;
import java.util.ArrayList;
import java.util.List;
import com.nuebus.servidorimpresion.presentacion.helpers.IExtractorBoletoService;
import com.nuebus.servidorimpresion.presentacion.services.impl.ImpresoraServiceImpl;

/**
 *
 * @author Valeria
 */
public class ExtractorBoletoConTokensServiceImpl implements IExtractorBoletoService{
    
    static private IImpresoraService impresoraService;
    
    public ExtractorBoletoConTokensServiceImpl(){
        ExtractorBoletoConTokensServiceImpl.impresoraService =  new ImpresoraServiceImpl() ;
    }  

    @Override
    public MessageBoleto extraerBoletosImpresion(byte[] datosZip) {     
        
        String datos = Utiles.unZipDatos(datosZip);          
        
        ///////Agrego final de pagina para compatibilidad/////////////
        datos += Utiles.TOKEN_PAGINA;        
        //Extrae los datos de la impresora fisica de la cabecera del paquete
        ImpresoraFisica impresora =  extraerImpresora( datos.substring(0, datos.indexOf(Utiles.TOKEN_LINEA))) ;
        
        datos = datos.substring( datos.indexOf(Utiles.TOKEN_LINEA) + 3 );      
        //Extraigo los boletos
        List<PageCanva> pages = extraerBoletos(datos);  
        
        
        MessageBoleto messageBoleto = new MessageBoleto( pages, impresora);
        
        return messageBoleto;
    }

    
    public  ImpresoraFisica extraerImpresora(String impresoraString){        
        
        final int ID_IMPRESORA_ORDEN = 1;
        final int ALTO_IMPRESORA_ORDEN = 2;        
        
        String emp_codigo =  Utiles.extraerCampo( impresoraString, ID_IMPRESORA_ORDEN, Utiles.TOKEN_CAMPO );
        int altoBoleto = Utiles.parseInt( Utiles.extraerCampo( impresoraString, 
                                                               ALTO_IMPRESORA_ORDEN, 
                                                               Utiles.TOKEN_CAMPO ) );        
        
        String  nombreImpresora = impresoraService.getNombreImpresora(emp_codigo);
        
        ImpresoraFisica impFisica =  impresoraService.search(nombreImpresora);
        impFisica.setAltoPagina(altoBoleto);      
        
        return impFisica;
    }
    
    
    public static List<PageCanva> extraerBoletos( String boletosString ){  
        
        List<PageCanva> pages = new ArrayList<>();            
        
        List<String> paginas = Utiles.splitByToken( boletosString, Utiles.TOKEN_PAGINA  );
        
        for( String pageString: paginas ){
            List<Elemento> elementos = new ArrayList<>();
            List<String> lineas = Utiles.splitByToken( pageString, Utiles.TOKEN_LINEA  );        
            
            lineas.forEach((lineaString) -> {
                elementos.add( fillElemento( lineaString ));
            });
            pages.add( new PageCanva(elementos));
        }    
      
        return pages;    
    }
    
    
    public static Elemento fillElemento( String elementoString ){               
        ElementoFactory elementoFactory = determinarFactory( determinarTipoElemento( elementoString ) );       
        Elemento elemento = elementoFactory.crea(elementoString);      
        return elemento;        
    } 
    
     public static TipoElementoEnum determinarTipoElemento( String elementoString ){
        int hastaCampo = -1;                    
        TipoElementoEnum tipo = TipoElementoEnum.TEXTO;        
        if((hastaCampo = elementoString.indexOf(Utiles.TOKEN_CAMPO)) >= 0){
                tipo = TipoElementoEnum.getTipoElementoById( elementoString.substring(0,hastaCampo));                 
        }
        return tipo;
    }
    
    public static ElementoFactory determinarFactory( TipoElementoEnum tipo ){
        ElementoFactory factory = null;
        if( null != tipo )switch (tipo) {
             case CODIGO_BARRA:
                 factory = new ElementoCodigoBarraFactory();
                 break;
             case CODIGO_PDF417:
                 factory = new ElementoCodigoPDFFactory();
                 break;
             case IMAGEN:
                 factory = new ElementoImagenFactory();
                 break;
             case TEXTO:
                 factory = new ElementoTextoFactory();
                 break;
             default:
                 break;
         }
        return factory;
    }      
   
}
