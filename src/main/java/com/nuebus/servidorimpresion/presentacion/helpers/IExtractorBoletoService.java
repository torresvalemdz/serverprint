package com.nuebus.servidorimpresion.presentacion.helpers;

import com.nuebus.servidorimpresion.negocio.ColaImpresion.MessageBoleto;
import com.nuebus.servidorimpresion.presentacion.services.IImpresoraService;

/**
 *
 * @author Valeria
 */
public interface IExtractorBoletoService {
     public MessageBoleto extraerBoletosImpresion( byte[] datosZip );
}
