package com.nuebus.servidorimpresion.presentacion.services;

import com.nuebus.servidorimpresion.entities.ImpresoraFisica;

/**
 *
 * @author Valeria
 */
public interface IImpresoraService {
    
    public ImpresoraFisica search( String nombreImpresora );   
    public ImpresoraFisica searchSinCache( String nombreImpresora ); 
    public boolean save( String nombreImpresora, ImpresoraFisica impresora );
    public String getNombreImpresora( String emp_codigo );
           
    
}
