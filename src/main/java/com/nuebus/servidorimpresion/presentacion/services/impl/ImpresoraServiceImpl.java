package com.nuebus.servidorimpresion.presentacion.services.impl;

import com.nuebus.servidorimpresion.acceso_datos.IImpresoraRepository;
import com.nuebus.servidorimpresion.acceso_datos.ImpresoraRepositoryImpl;
import com.nuebus.servidorimpresion.entities.ImpresoraFisica;
import com.nuebus.servidorimpresion.presentacion.services.IImpresoraService;
import com.nuebus.servidorimpresion.utiles.UsuarioException;
import com.nuebus.servidorimpresion.utiles.Utiles;

/**
 *
 * @author Valeria
 */
public class ImpresoraServiceImpl implements IImpresoraService{
    
    private static IImpresoraRepository impresoraRepository = new ImpresoraRepositoryImpl();

    public ImpresoraServiceImpl(){
       ImpresoraServiceImpl.impresoraRepository = new ImpresoraRepositoryImpl();
    }
    
    @Override
    public ImpresoraFisica search( String nombreImpresora ) {
        return impresoraRepository.search(nombreImpresora );
    }

    @Override
    public boolean save(String nombreImpresora, ImpresoraFisica impresora) {       
        
        boolean todoOK = false;        
        
        if( !impresora.validate() ){
           throw new UsuarioException( "Validacion Impresora" 
                                        +  Utiles.parseError( impresora.getErrores(), ";" )); 
        }
        
        todoOK = impresoraRepository.save(nombreImpresora, impresora);        
        return todoOK;        
    }
    
    @Override
    public  ImpresoraFisica searchSinCache(String nombreImpresora){       
        return impresoraRepository.searchSinCache(nombreImpresora);
    }
    
    public String getNombreImpresora( String emp_codigo ){
        return "Imp"+ emp_codigo;
    }

    
}
