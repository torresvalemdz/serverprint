package com.nuebus.servidorimpresion.presentacion.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import com.nuebus.servidorimpresion.logs.Logger;


/**
 *
 * @author Valeria
 */
public class ServidorWeb {

    public static final int PUERTO = 50000;
    public static final String MJE_ARRANQUE_OK = "CONF: Escuchando en el puerto " 
                                                + ServidorWeb.PUERTO 
                                                + " ...\n";  
    
    
    public void arranca( ){
        
        boolean bienvenida=true;
        try{            
           ServerSocket servidor = new ServerSocket(PUERTO);           
           while( true ){
                
                if( bienvenida ){
                  Logger.getLogger().loggearArranqueOK(MJE_ARRANQUE_OK );
                  bienvenida= false;
                }
               
                Socket cliente = servidor.accept();
                PeticionWeb pCliente = new PeticionWeb(cliente);
                pCliente.start();            
           }
        }catch(java.net.SocketException e){
            Logger.getLogger().error("Puerto " +  PUERTO + " Ocupado");                        
        }catch(IOException e){
            Logger.getLogger().error("IOException servidorWeb.arranca " + e.getMessage());            
        }      
        
    }

   
}
