
package com.nuebus.servidorimpresion.presentacion.server;

import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import com.nuebus.servidorimpresion.logs.Logger;
import com.nuebus.servidorimpresion.presentacion.pagesManagement.PageFactoryEnum;
import com.nuebus.servidorimpresion.presentacion.pagesManagement.PageWeb;
import com.nuebus.servidorimpresion.utiles.UsuarioException;
import java.io.IOException;

/**
 *
 * @author Valeria
 */
public class PeticionWeb extends Thread {
    
    private InputStream input = null; // representa la petici�n de nuestro cliente
    private PrintWriter out = null; // representa el buffer donde escribimos la respuesta    
    
    
    public PeticionWeb(Socket ps){
        
        try {
            input = ps.getInputStream();
            out = new PrintWriter(ps.getOutputStream());
        } catch (IOException ex) {
            Logger.getLogger().error("peticionWeb " + ex.getMessage() );        
        } finally{   
            setPriority(NORM_PRIORITY - 1); // hacemos que la prioridad sea baja
        }    
    }
    
    public void run() {
        try {                                

            String pagina = getPagina(input);                       
            
            int tamanioDatos = getTamanioDatos(input);               
            
            byte[] file  = new byte[ tamanioDatos ];
            input.read( file );            
            
            PageWeb paginaWeb = PageFactoryEnum.PAGE_FACTORY.getPaginaWeb( pagina, out, file );
            
            if( paginaWeb != null  ){
                paginaWeb.generarResultado(); 
            }else{
                Logger.getLogger().error(" Page Not Found - " + pagina);
                paginaWeb.pageNotFound(out);
            }            
            
            out.close();            
            input.close();
            
        } catch(IOException e){
            Logger.getLogger().error("IOException peticionWeb run " + e.getMessage());
        } catch( UsuarioException ex ){
            //Logger.error("peticionWeb run " + ex.getMessage());
        } catch( Exception ex){
            Logger.getLogger().error("peticionWeb run " + ex.getMessage());
        }    
    }
    
     private String  getPagina( InputStream input ){        
        String pagina="";                
        try{
            byte[] content = new byte[1];
            while( ( input.read( content ) != -1 ) && content[0]!= 13 ) {                             
                pagina +=String.valueOf((char)content[0]);                
            }
            
         pagina =  pagina.substring( pagina.indexOf("/")+1 , pagina.indexOf(" ",pagina.indexOf("/")+1 ) );
            
        }catch( IOException ex){
           throw new UsuarioException("IOException getPagina ", ex);
        }    
        return pagina;
    } 
    
     private int   getTamanioDatos( InputStream input ){  
        int tamanioDatos = 0;
         
        try{
            byte[] content = new byte[1];
            boolean datos = false;            
            byte []control = new byte[3];
            control[0]=0;
            control[1]=0;
            control[2]=0;
            String line = "";
            while( (  input.read( content )  != -1 ) && !datos ) {
                
                if( content[0] == 13 ){
                    if( line != null && line.indexOf("Content-Length:") > -1) {
                            tamanioDatos = new Integer(
                                    line.substring(  line.indexOf("Content-Length:") + 16,
                                                   line.length())).intValue();
                    }                    
                    line = "";
                }               
                
                line +=String.valueOf((char)content[0]);                    
                control[0] = control[1];
                control[1] = control[2];
                control[2] = content[0];
                if(  control[0]==13
                     &&   control[1]==10
                     &&   control[2]==13) {             
                    datos =true;
                }
             }          
            
        }catch(IOException ex){
            Logger.getLogger().error("IOException getTamanioDatos " + ex.getMessage() );
        }
         
        return tamanioDatos;       
    }    
    
}
