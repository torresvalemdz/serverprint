
package com.nuebus.servidorimpresion.presentacion.pagesManagement;

/**
 *
 * @author Valeria
 */
public enum GuardarImpresoraEnum {
        emp_codigo(true),
        impresora(true),
        tipoDeLetra(true),
        tamanio(true),
        margenIzquierdo(true),
        margenSuperior(true),
        orientacion(true)   ;

    private GuardarImpresoraEnum( boolean obligatorio ){
        this.obligatorio = obligatorio;
    }   
    
    boolean obligatorio;

    public boolean isObligatorio() {
        return obligatorio;
    }  
}
