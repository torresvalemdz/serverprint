package com.nuebus.servidorimpresion.presentacion.pagesManagement;


import com.google.gson.JsonObject;
import com.nuebus.servidorimpresion.logs.Logger;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Properties;
import javax.swing.JTextField;
import com.nuebus.servidorimpresion.utiles.UsuarioException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Del Valle
 */
public abstract class PageWeb {
    
    PrintWriter out;
    byte[] parametros;
    public static final String CHARSET = "ISO-8859-1";
    JTextField msgVisor = null;

    public static final int OK = 1;
    public static final int ERROR = 2;
    
    /** Creates a new instance of PaginaWeb */

    public PageWeb( ) {
    }

    public PageWeb( PrintWriter out, byte[] parametros) {
        this.out = out;
        this.parametros = parametros;
    }

    public abstract void generarResultado( );

   

    public String  getCabeceraOk(){
        return  "HTTP/1.1 200 OK" + "\r\n"
                + "Content-Type: text/html;charset=" + CHARSET + "\r\n"
                + "Access-Control-Allow-Origin:*" +"\r\n"
                + "P3P: CP=\"NOI ADM DEV PSAi COM NAV OUR OTR STP IND DEM\"";
        
    }

      public String  getCabeceraOkXML(){
        return  "HTTP/1.1 200 OK" + "\r\n"
                + "Content-Type: text/xml;charset=" + CHARSET + "\r\n"
                + "Access-Control-Allow-Origin:*" +"\r\n"
                + "P3P: CP=\"NOI ADM DEV PSAi COM NAV OUR OTR STP IND DEM\"";

    }
      
      
    public String  getCabeceraOkJson(){
        return  "HTTP/1.1 200 OK" + "\r\n"
                + "Content-Type:application/json;charset=" + CHARSET + "\r\n"
                + "Access-Control-Allow-Origin:*" +"\r\n"
                + "P3P: CP=\"NOI ADM DEV PSAi COM NAV OUR OTR STP IND DEM\"";
        
    }  
    
    public static  void  pageNotFound( PrintWriter out ){
        
        out.println("HTTP/1.1 404 " + "\r\n"
                + "Content-Type: text/html;charset=" + CHARSET + "\r\n");                             
        
        out.println("");
        out.println("<html><h3>Error 404.Page Not Found </h3></html> ");
        
    } 
    
    public Properties getParametros(String data) {
        String separadorValor = "=";
        String separadorCampos = "&";
        Properties parametros = new Properties();
        if( data != null ){
             while( data.length() > 0 ){
                 String campo = "";
                 String valor = "";
                 int posValor = data.indexOf(separadorValor);
                 if( posValor > -1 &&  posValor <= data.length() ){
                     campo = data.substring(0, posValor);
                     if( campo != null && campo.length()> 0){
                        int posCampo =  data.indexOf(separadorCampos);

                        try{
                            if( posCampo > -1 && posCampo <= data.length()){
                                valor = URLDecoder.decode(data.substring(posValor + 1 ,posCampo), PageWeb.CHARSET);
                                data = data.substring(posCampo +1 , data.length());
                            }else{
                                //Es el ultimo parametro
                                valor = URLDecoder.decode(data.substring(posValor + 1 , data.length()), PageWeb.CHARSET);
                                data = "";
                            }
                        }catch(UnsupportedEncodingException ex){                                                      
                            valor = "";
                            Logger.getLogger().error("UnsupportedEncodingException getParametros " + ex.getMessage());                           
                        }                      
                        parametros.put(campo, valor);
                     }
                 }
             }
        }

        return parametros;
    }
    
    protected JsonObject getJsonRespuesta(int codigo, String mensaje){
          JsonObject jsonResp = new JsonObject(); 
          jsonResp.addProperty("codigo",codigo);
          jsonResp.addProperty("mensaje", mensaje);
          return jsonResp;   
    }

    public List<String>validarParametros( Properties proParametros ){
        return new ArrayList<>();
    }
    
    public JsonObject generarRespuestaError( List<String> errores ){         
        if(!errores.isEmpty()){
             String erroresString= "";
             erroresString = errores.stream().map((error) -> error+";")
                     .reduce(erroresString, String::concat);
             return getJsonRespuesta(PageWeb.ERROR, erroresString );
        } 
        return new JsonObject();
    }
    
}    
