

package com.nuebus.servidorimpresion.presentacion.pagesManagement;

import java.io.PrintWriter;
import com.nuebus.servidorimpresion.negocio.ColaImpresion.ProducerConsumerService;
import com.nuebus.servidorimpresion.logs.Logger;
import com.nuebus.servidorimpresion.negocio.ColaImpresion.MessageBoleto;
import com.nuebus.servidorimpresion.presentacion.helpers.impl.ExtractorBoletoConTokensServiceImpl;
import com.nuebus.servidorimpresion.presentacion.helpers.IExtractorBoletoService;



/**
 *
 * @author Valeria
 */
public class ImprimirPage extends PageWeb {    
  
    static public String NOMBRE_PAGINA = "imprimir.html";
    
    IExtractorBoletoService  boletoService; 
     /*.setImpresoraService( new ImpresoraServiceImpl() );*/

    public ImprimirPage( PrintWriter out, byte[] parametros ) {
       super(out, parametros);    
       
       boletoService = new ExtractorBoletoConTokensServiceImpl();       
    }

    @Override
    public void generarResultado(){
        
        out.println(getCabeceraOkJson());
        out.println("");

        
        if( parametros.length > 0  ){
           
           MessageBoleto mjeBoleto = boletoService.extraerBoletosImpresion(parametros);
            
           if(  mjeBoleto.validateImpresora() && mjeBoleto.validateBoletos() ){               

                if(ProducerConsumerService.getInstance()!= null){
                    ProducerConsumerService.getInstance().produce(mjeBoleto);                
                }   
                Logger.getLogger().info("Imprimiendo Boletos");              
                out.println(getJsonRespuesta(PageWeb.OK, "Todo Ok." ));  
                
           }else if( !mjeBoleto.validateImpresora() ){  
               
                Logger.getLogger().error("Debe definir la impresora de Trabajo.");
                out.println(getJsonRespuesta(PageWeb.ERROR, "Debe definir la impresora de Trabajo.<br><b>Opc. Menu: /Encargado de  Agencia/Procesos Especiales/Panel de Control de Impresoras.<b> " ));             
                
           }else if( !mjeBoleto.validateBoletos() ){                              
                Logger.getLogger().error("Error al recibir los boletos desde el servidor.Longitud (0)");
                out.println(getJsonRespuesta(PageWeb.ERROR, "No se recuperaron correctamente los boletos del servidor.Longitud (0)" ));                             
           }else{                        
                Logger.getLogger().error("Error Inesperado");
                out.println(getJsonRespuesta(PageWeb.ERROR, "Error inesperado en el servidor de impresion" ));                    
           }                  
        
        }else if( parametros.length <= 0){           
            
            Logger.getLogger().error("Error al recibir los boletos desde el servidor.");
            out.println(getJsonRespuesta(PageWeb.ERROR, "No se recuperaron correctamente los boletos del servidor" ));              
        }      
        
    }  

    
}
