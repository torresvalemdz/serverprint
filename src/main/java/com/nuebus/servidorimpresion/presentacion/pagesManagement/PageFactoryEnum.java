package com.nuebus.servidorimpresion.presentacion.pagesManagement;

import java.io.PrintWriter;

/**
 *
 * @author Valeria
 */
public enum PageFactoryEnum {
    
    PAGE_FACTORY;
    
     public static PageWeb  getPaginaWeb( String paginaNombre, PrintWriter out, byte[] parametros ){
        PageWeb pagina = null;
        if( paginaNombre.equalsIgnoreCase(ImprimirPage.NOMBRE_PAGINA)){
             pagina =  new ImprimirPage( out, parametros );
        }else if(paginaNombre.equalsIgnoreCase(DatosImpresoraPage.NOMBRE_PAGINA)){
             pagina = new DatosImpresoraPage( out, parametros );       
        } else if( paginaNombre.equalsIgnoreCase(GuardarImpresoraPage.NOMBRE_PAGINA) ){
            pagina = new GuardarImpresoraPage(out,parametros) ;
        }        
        return pagina;
    }
    
}
