
package com.nuebus.servidorimpresion.presentacion.pagesManagement;

import com.google.gson.JsonObject;
import java.io.PrintWriter;
import java.util.Properties;
import com.nuebus.servidorimpresion.entities.ImpresoraFisica;
import com.nuebus.servidorimpresion.logs.Logger;
import com.nuebus.servidorimpresion.presentacion.services.IImpresoraService;
import com.nuebus.servidorimpresion.presentacion.services.impl.ImpresoraServiceImpl;
import com.nuebus.servidorimpresion.utiles.UsuarioException;
import com.nuebus.servidorimpresion.utiles.Utiles;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Valeria
 */
public class GuardarImpresoraPage extends PageWeb {
    
    IImpresoraService impresoraService = new ImpresoraServiceImpl();

    static public String NOMBRE_PAGINA = "guardarImpresora.html";

    public GuardarImpresoraPage(PrintWriter out, byte[] parametros ) {
       super(out, parametros);
    }

    @Override
    public void generarResultado() {   
         
         out.println( getCabeceraOkJson());
         out.println("");        
         out.println( getHtml() );
    }
    
    public JsonObject getHtml(){
        
         Properties proParametros =  getParametros( new String(parametros));         
         List<String> erroresParametros = validarParametros(proParametros);         
         if( !erroresParametros.isEmpty() ){ 
             Logger.getLogger().error( "Guardar Impresora[ " + Utiles.parseError(erroresParametros, "," ) + "]" );
             return generarRespuestaError(erroresParametros);
         }
         
         if( proParametros.getProperty(GuardarImpresoraEnum.emp_codigo.name()) != null ){             
             String nombreImpresora = impresoraService.getNombreImpresora( proParametros.getProperty("emp_codigo"));            
             
             ImpresoraFisica impFisica = new ImpresoraFisica();

             impFisica.setImpresora(proParametros.getProperty(GuardarImpresoraEnum.impresora.name()));
             impFisica.setTipoDeLetra(proParametros.getProperty(GuardarImpresoraEnum.tipoDeLetra.name()));
             impFisica.setTamanio(Utiles.parseInt(proParametros.getProperty(GuardarImpresoraEnum.tamanio.name())));
             impFisica.setMargenIzquierdo(Utiles.parseInt(proParametros.getProperty(GuardarImpresoraEnum.margenIzquierdo.name())));
             impFisica.setMargenSuperior(Utiles.parseInt(proParametros.getProperty(GuardarImpresoraEnum.margenSuperior.name())));
             impFisica.setOrientacion(Utiles.parseInt(proParametros.getProperty(GuardarImpresoraEnum.orientacion.name())));
             //impFisica.setAltoPagina(altoPagina) -> se manejara en tiempo de ejecucion////

            
             try{
                if( impresoraService.save(nombreImpresora, impFisica) ){                     
                   return getJsonRespuesta(PageWeb.OK, "TODO oK." );  
                }                 
             }catch( UsuarioException ex ){
               return getJsonRespuesta(PageWeb.ERROR, ex.getMessage() );     
             }                      

         }       
         return null;
    }

   @Override
   public  List<String> validarParametros( Properties proParametros ) {
        List<String> errores = new ArrayList<>();        
        for( GuardarImpresoraEnum dato: GuardarImpresoraEnum.values() ){
            if( !proParametros.containsKey( dato.name() ) && dato.isObligatorio() ){
                errores.add("Debe espeficar " + dato.name() );
            }
        }        
        return errores;
    }   
    
    
    

}
