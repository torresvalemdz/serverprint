package com.nuebus.servidorimpresion.presentacion.pagesManagement;

/**
 *
 * @author Valeria
 */
public enum DatosImpresoraEnum {
    emp_codigo(true);
    
    DatosImpresoraEnum( boolean obligatorio ){
        this.obligatorio = obligatorio;
    }
    boolean obligatorio; 

    public boolean isObligatorio() {
        return obligatorio;
    }
    
}
