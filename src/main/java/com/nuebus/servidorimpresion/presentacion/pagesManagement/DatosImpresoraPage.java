
package com.nuebus.servidorimpresion.presentacion.pagesManagement;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.awt.GraphicsEnvironment;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Properties;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import com.nuebus.servidorimpresion.entities.ImpresoraFisica;
import com.nuebus.servidorimpresion.presentacion.services.IImpresoraService;
import com.nuebus.servidorimpresion.presentacion.services.impl.ImpresoraServiceImpl;
import com.nuebus.servidorimpresion.utiles.UsuarioException;
import com.nuebus.servidorimpresion.versionado.VersionProducto;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Valeria
 */
public class DatosImpresoraPage extends PageWeb {
    
    static IImpresoraService impresoraService = null;

    static public String NOMBRE_PAGINA = "datosImpresora.html";

    public DatosImpresoraPage(PrintWriter out, byte[] parametros ) {
       super(out, parametros);
       DatosImpresoraPage.impresoraService = new ImpresoraServiceImpl();
    }


    @Override
    public void generarResultado() {       
        out.println(getCabeceraOkJson());       
        //out.println( identificacion + impresoras + tpLetras );       
        out.println("");        
        out.println(getHTML());      
    }

    
     public JsonObject getHTML(){        
        JsonObject imp = new JsonObject(); 
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();

          Properties proParametros =  getParametros( new String(parametros));          
          
          List<String> erroresParametros = validarParametros(proParametros);         
          if( !erroresParametros.isEmpty() ){ return generarRespuestaError(erroresParametros);}
         
          
          imp.addProperty("version", VersionProducto.getVersion());    
          
          ImpresoraFisica impFisica = null;
          if( proParametros.getProperty(DatosImpresoraEnum.emp_codigo.name()) != null ){
             try{
                String nombreImpresora = impresoraService.getNombreImpresora(proParametros.getProperty(DatosImpresoraEnum.emp_codigo.name()));
                impFisica = impresoraService.search( nombreImpresora );          
             }catch( UsuarioException ex ){                 
               return getJsonRespuesta(PageWeb.ERROR, ex.getMessage() );     
             }          
              
          }
          
          imp.add("impresora", impresoraFisicaToJson(impFisica));          
          // altoPagina= 0; -> es dinamico        
          imp.add("orientacion", orientacionToJson());
          
          PrintService services [] = PrintServiceLookup.lookupPrintServices(null,null);         
          List<String> impresoras= new ArrayList<>();          
          for(int i = 0; i < services.length; i++ ){
             impresoras.add(services[i].getName());
          }          
          
          imp.add("impresoras", gson.toJsonTree(impresoras));
          
          List<String> tipoLetras= new ArrayList<>();          
          GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
          String letras[]= env.getAvailableFontFamilyNames();
          tipoLetras.addAll(Arrays.asList(letras));
          imp.add("letras", gson.toJsonTree(tipoLetras));                    
        
        return imp;        
    }

    private JsonObject impresoraFisicaToJson( ImpresoraFisica impFisica ){        
        
        if( impFisica == null ){
            impFisica = new ImpresoraFisica();
        }                
        JsonObject imp = new JsonObject(); 
        imp.addProperty("impresora", impFisica.getImpresora());
        imp.addProperty("tipoDeLetra", impFisica.getTipoDeLetra());
        imp.addProperty("tamanio", impFisica.getTamanio());
        imp.addProperty("margenIzquierdo", impFisica.getMargenIzquierdo());
        imp.addProperty("margenSuperior", impFisica.getMargenSuperior());
        imp.addProperty("orientacion", impFisica.getOrientacion());
        
        return imp;
        
    }

    private JsonObject orientacionToJson(){        
        JsonObject orientacion = new JsonObject(); 
        orientacion.addProperty(String.valueOf(ImpresoraFisica.HORIZONTAL),ImpresoraFisica.STR_HORIZONTAL);
        orientacion.addProperty(String.valueOf(ImpresoraFisica.VERTICAL),ImpresoraFisica.STR_VERTICAL);
        return orientacion;   
    }

    @Override
    public List<String> validarParametros( Properties proParametros ) {      
        
        List<String> errores = new ArrayList<>();        
        for( DatosImpresoraEnum dato: DatosImpresoraEnum.values() ){
            if( !proParametros.containsKey( dato.name() ) && dato.isObligatorio() ){
                errores.add("Debe espeficar " + dato.name() );
            }
        }        
        return errores;        
    }  
    
    
}
