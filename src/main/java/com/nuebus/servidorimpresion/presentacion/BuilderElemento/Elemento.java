package com.nuebus.servidorimpresion.presentacion.BuilderElemento;

import com.nuebus.servidorimpresion.negocio.impresion.Printer;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.MediaTracker;


public class Elemento {    
    
    TipoElementoEnum tipo;
    EstiloElementoEnum estilo;
    int top = 0;
    int left = 0;
    int sizeLetra = 0;
    String text = "";
    
    double escalaAncho = 0;
    double escalaAlto = 0;
    
    Font normal = null;
    Font negrita = null;        

    public Elemento() {
    }     

    public Elemento(TipoElementoEnum tipo) {
        this.tipo = tipo;
    }    

    public int getSizeLetra() {
        return sizeLetra;
    }

    public void setSizeLetra(int sizeLetra) {
        this.sizeLetra = sizeLetra;
    }

    public EstiloElementoEnum getEstilo() {
        return estilo;
    }

    public void setEstilo(EstiloElementoEnum estilo) {
        this.estilo = estilo;
    }   
   
    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TipoElementoEnum getTipo() {
        return tipo;
    }

    public void setTipo(TipoElementoEnum tipo) {
        this.tipo = tipo;
    }    

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public Font getNormal() {
        return normal;
    }

    public void setNormal(Font normal) {
        this.normal = normal;
    }

    public Font getNegrita() {
        return negrita;
    }

    public void setNegrita(Font negrita) {
        this.negrita = negrita;
    }

    public double getEscalaAncho() {
        return escalaAncho;
    }

    public void setEscalaAncho(double escalaAncho) {
        this.escalaAncho = escalaAncho;
    }

    public double getEscalaAlto() {
        return escalaAlto;
    }

    public void setEscalaAlto(double escalaAlto) {
        this.escalaAlto = escalaAlto;
    }     
            
    public void dibujar( Graphics2D g2d,  MediaTracker mt, Printer printer ){}

}
