
package com.nuebus.servidorimpresion.presentacion.BuilderElemento;

import com.nuebus.servidorimpresion.utiles.Utiles;

/**
 *
 * @author Valeria
 */
public abstract class ElementoFactory {
    
    public Elemento crea( String elementoString ){
        //Cada linea debe tener 5 TOKEN_CAMPO sino los relleno con vacio
        
        Elemento elemento = creaElemento();        
         

         elemento.setEstilo( EstiloElementoEnum.getEstiloElementoById(
                                Utiles.parseInt( Utiles.extraerCampo( elementoString, 
                                                                      OrdenElementoEnum.ESTILO.getOrden(), 
                                                                      Utiles.TOKEN_CAMPO ) ) ) );                 

         elemento.setTop(Utiles.parseInt( Utiles.extraerCampo( elementoString,
                                                                 OrdenElementoEnum.TOP.getOrden(), 
                                                                 Utiles.TOKEN_CAMPO ) ) );

         elemento.setLeft(Utiles.parseInt( Utiles.extraerCampo( elementoString,
                                                                  OrdenElementoEnum.LEFT.getOrden(),
                                                                  Utiles.TOKEN_CAMPO ) ));

         elemento.setSizeLetra(Utiles.parseInt( 
                                          Utiles.extraerCampo( elementoString, 
                                                               OrdenElementoEnum.SIZE_LETRA.getOrden(),
                                                               Utiles.TOKEN_CAMPO ) ));                  

         elemento.setText( Utiles.extraerCampo( elementoString,
                                                  OrdenElementoEnum.TEXT.getOrden(),
                                                  Utiles.TOKEN_CAMPO ) );
         
         elemento.setEscalaAncho( Utiles.parseDouble(
                                            Utiles.extraerCampo( elementoString,
                                                                 OrdenElementoEnum.ESCALADA_ANCHO.getOrden(),
                                                                 Utiles.TOKEN_CAMPO ) ) );
         
         
          elemento.setEscalaAlto(Utiles.parseDouble(
                                            Utiles.extraerCampo( elementoString,
                                                                 OrdenElementoEnum.ESCALADA_ALTO.getOrden(),
                                                                 Utiles.TOKEN_CAMPO ) ) );
        
        return elemento;
    }    
    
    protected abstract Elemento creaElemento();
    
}
