package com.nuebus.servidorimpresion.presentacion.BuilderElemento;

import com.nuebus.servidorimpresion.logs.Logger;
import com.nuebus.servidorimpresion.negocio.impresion.Printer;
import com.nuebus.servidorimpresion.utiles.Utiles;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.net.URL;
import java.util.UUID;

/**
 *
 * @author Valeria
 */
public class ElementoImagen extends Elemento{

    public ElementoImagen() {
        super( TipoElementoEnum.IMAGEN );
    }   
    
    
    @Override
    public void dibujar( Graphics2D g2d,  MediaTracker mt, Printer printer ) {
        
        long x = Math.round( getLeft()* Utiles.PULGADA );
        long y = Math.round( getTop() * Utiles.PULGADA );
        
        Image imagen;
        try {
           int id = UUID.randomUUID().hashCode();           
           imagen = Toolkit.getDefaultToolkit().getImage(new URL( getText()));
           mt.addImage(imagen,id);
           mt.waitForID(id);

           AffineTransform tranformador = new AffineTransform();
           tranformador.translate((int)x , (int)(y - imagen.getHeight(printer)) );
           tranformador.scale(getEscalaAncho(),getEscalaAlto());

           g2d.drawImage(imagen,tranformador, null);
        } catch (Exception ex) {
           Logger.getLogger().error("Error dibujar " + ex.getMessage());
        }
    }  
    
}
