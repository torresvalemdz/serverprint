package com.nuebus.servidorimpresion.presentacion.BuilderElemento;

/**
 *
 * @author Valeria
 */
public class ElementoCodigoBarraFactory extends ElementoFactory{

    @Override
    protected Elemento creaElemento() {
        return new ElementoCodigoBarra();
    }
    
}
