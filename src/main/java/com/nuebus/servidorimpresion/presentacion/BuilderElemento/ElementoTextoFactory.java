
package com.nuebus.servidorimpresion.presentacion.BuilderElemento;

/**
 *
 * @author Valeria
 */
public class ElementoTextoFactory extends ElementoFactory{

    @Override
    protected Elemento creaElemento() {
        return new ElementoTexto();
    }
    
}
