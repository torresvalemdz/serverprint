package com.nuebus.servidorimpresion.presentacion.BuilderElemento;

/**
 *
 * @author Valeria
 */
public class ElementoCodigoPDFFactory extends ElementoFactory{

    @Override
    protected Elemento creaElemento() {
        return new ElementoCodigoPDF();
    }
    
}
