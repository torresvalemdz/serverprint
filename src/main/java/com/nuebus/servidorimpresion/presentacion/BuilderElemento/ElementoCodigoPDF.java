package com.nuebus.servidorimpresion.presentacion.BuilderElemento;

import com.nuebus.servidorimpresion.negocio.impresion.BarCode;
import com.nuebus.servidorimpresion.negocio.impresion.BarCodePDF417;
import com.nuebus.servidorimpresion.negocio.impresion.Printer;
import com.nuebus.servidorimpresion.utiles.Utiles;
import java.awt.Graphics2D;
import java.awt.MediaTracker;
import java.awt.geom.AffineTransform;

/**
 *
 * @author Valeria
 */
public class ElementoCodigoPDF extends Elemento{

    public ElementoCodigoPDF() {
        super( TipoElementoEnum.CODIGO_PDF417 );
    }
    
    @Override
    public void dibujar( Graphics2D g2d,  MediaTracker mt, Printer printer ) {
        long x = Math.round( getLeft()* Utiles.PULGADA );
        long y = Math.round( getTop() * Utiles.PULGADA );
        
        BarCode barCode = new BarCodePDF417();
        g2d.setFont(normal);
        AffineTransform tranformador = new AffineTransform();
        tranformador.translate((int)x , (int)(y - 31));
        tranformador.scale(0.7,0.9);
        g2d.drawImage(barCode.getBarcode( getText() ),tranformador, null);
    }  
    
}
