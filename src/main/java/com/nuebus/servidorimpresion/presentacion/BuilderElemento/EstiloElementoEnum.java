package com.nuebus.servidorimpresion.presentacion.BuilderElemento;

/**
 *
 * @author Valeria
 */
public enum EstiloElementoEnum {
    
    NORMAL(0), NEGRITA(1);
    
    int id=0;
    
    private EstiloElementoEnum( int id ){
        this.id = id;
    } 

    public int getId() {
        return id;
    }
    
    public static EstiloElementoEnum getEstiloElementoById( int id ){        
        EstiloElementoEnum retorno = EstiloElementoEnum.NORMAL;        
        if( id == EstiloElementoEnum.NEGRITA.getId() ){
            retorno = EstiloElementoEnum.NEGRITA;
        }
        return retorno;
    }
    
    
    /*static final int NORMAL = 0;
    static final int NEGRITA = 1;*/
    
}
