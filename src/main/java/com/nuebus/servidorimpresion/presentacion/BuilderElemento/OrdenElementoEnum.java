
package com.nuebus.servidorimpresion.presentacion.BuilderElemento;

/**
 *
 * @author Valeria
 */
public enum OrdenElementoEnum {
    TIPO(1), ESTILO(2), TOP(3), LEFT(4), SIZE_LETRA(5), TEXT(6),
    ESCALADA_ANCHO(7), ESCALADA_ALTO(8);
    
    private int orden;

    private OrdenElementoEnum(int orden) {
        this.orden = orden;
    }

    public int getOrden() {
        return orden;
    }    
    
}
