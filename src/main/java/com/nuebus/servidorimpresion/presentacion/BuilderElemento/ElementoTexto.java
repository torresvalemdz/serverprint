package com.nuebus.servidorimpresion.presentacion.BuilderElemento;

import com.nuebus.servidorimpresion.negocio.impresion.Printer;
import com.nuebus.servidorimpresion.utiles.Utiles;
import java.awt.Graphics2D;
import java.awt.MediaTracker;

/**
 *
 * @author Valeria
 */
public class ElementoTexto extends Elemento {

    public ElementoTexto() {
        super( TipoElementoEnum.TEXTO );
    }

    @Override
    public void dibujar( Graphics2D g2d,  MediaTracker mt, Printer printer ) {
        
        long x = Math.round( getLeft()* Utiles.PULGADA );
        long y = Math.round( getTop() * Utiles.PULGADA );
        
        g2d.setFont(normal);
        if( getEstilo() == EstiloElementoEnum.NEGRITA ){
           g2d.setFont(negrita);                           
        }
        g2d.drawString( getText(),(int)x , (int)y );
    }  
    
}
