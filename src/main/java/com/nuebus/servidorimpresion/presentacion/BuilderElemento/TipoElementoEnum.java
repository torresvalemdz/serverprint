package com.nuebus.servidorimpresion.presentacion.BuilderElemento;
/**
 *
 * @author Valeria
 */
public enum TipoElementoEnum {  
    
    CODIGO_BARRA("B"), TEXTO("S"), IMAGEN("I"), CODIGO_PDF417("P"); 
    
    String id = "";
    
    private TipoElementoEnum( String id ){
        this.id = id;
    }   

    public String getId() {
        return id;
    }    
    
    static public TipoElementoEnum getTipoElementoById( String id){
        
        TipoElementoEnum retorno = TEXTO;
        
        if( id.equalsIgnoreCase( CODIGO_BARRA.getId() ) ){
            retorno = CODIGO_BARRA;
        }else if(  id.equalsIgnoreCase( TEXTO.getId() ) ){
            retorno = TEXTO;
        }else if( id.equalsIgnoreCase( IMAGEN.getId() ) ){
            retorno = IMAGEN;
        }else if( id.equalsIgnoreCase( CODIGO_PDF417.getId() ) ){
            retorno = CODIGO_PDF417;
        }
        return retorno;
    }
    
     /*
        static final String CODIGO_BARRA = "B";
        static final String ELEMENTO = "S";
        static final String IMAGEN = "I";
        static final String CODIGO_PDF417 = "P";
        static final String IMAGEN_QR = "Q";
    */
    
}
