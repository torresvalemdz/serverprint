
package com.nuebus.servidorimpresion.presentacion.BuilderElemento;

/**
 *
 * @author Valeria
 */
public class ElementoImagenFactory extends ElementoFactory{

    @Override
    protected Elemento creaElemento() {
        return new ElementoImagen();
    }
    
}
