
package com.nuebus.servidorimpresion.presentacion.pantallas;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.KeyStroke;

/** Establece propiedades comunes a las ventanas que se abren en la aplicacion. */
public abstract class JIFrame extends JInternalFrame {

//==CONSTRUCTOR ===============================================================
    /** Crea un JInternalFrame sin titulo, sin icono*/
    public JIFrame() {
         setFrameIcon(null);
         
         setTitle(null);

         setResizable(false);
         setClosable(true);
         
         //Make the big window be indented 2 pixels from each edge of the screen.
         int inset = 1;
         Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
         setBounds(inset, inset,screenSize.width - inset*17 , screenSize.height - inset*80 );
        
         //escucharTecla();
	 }
    
    public  void iniciarPantalla(){};
    
    public void escucharTecla() { 
        //La tecla a usar 
        KeyStroke keyX = KeyStroke.getKeyStroke(KeyEvent.VK_X, 0);
        
        //La Action que hace el trabajo 
         AbstractAction cerrarVentana = new AbstractAction() { 
         	  public void actionPerformed(ActionEvent e) {
         	       dispose();
         	  } 
         };
        
        // Hay que usar el InputMap del RootPane para que afecte a toda la ventana.  
        // Asignamos una KeyStroke al InputMap y le ponemos una etiqueta 
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(keyX, "cerrarVentana");
        
        //Asignamos una Action a la etiqueta definida 
        getRootPane().getActionMap().put("cerrarVentana", cerrarVentana );
        
    } 
}
//==FIN DE LA CLASE ===========================================================