
package com.nuebus.servidorimpresion.presentacion.pantallas;

/**
 *
 * @author Valeria
 */

import com.nuebus.servidorimpresion.logs.Logger;
import com.nuebus.servidorimpresion.logs.LoggerFisico;
import com.nuebus.servidorimpresion.utiles.Utiles;
import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Icon {

    private JFrame miframe;
    private PopupMenu popup = new PopupMenu();
    private Image image =  Utiles.getImageIcono();;
    private final TrayIcon trayIcon = new TrayIcon(image, "Servidor de Impresi�n Nuebus", popup);
   

 public Icon( JFrame frame) throws IOException{    
    this.miframe = frame;
    //comprueba si SystemTray es soportado en el sistema
    if (SystemTray.isSupported())   {

            //obtiene instancia SystemTray
            SystemTray systemtray = SystemTray.getSystemTray();
            //acciones del raton sobre el icono en la barra de tareas
            MouseListener mouseListener = new MouseListener() {

              public void mouseClicked(MouseEvent evt) {                                 
                  /*//Si se presiono el boton izquierdo y la aplicacion esta minimizada
                  if( evt.getClickCount() == 1 && evt.getButton() == MouseEvent.BUTTON1 && miframe.getExtendedState()==JFrame.ICONIFIED ){
                      MensajeTrayIcon("Nuebus Venta de Pasajes", MessageType.INFO);
                  }else if ( evt.getClickCount() >= 2){
                      miframe.setVisible(true);
                      miframe.setExtendedState( JFrame.NORMAL );
                      miframe.repaint();
                  }*/                  
                  miframe.setVisible(true);
                  miframe.setExtendedState( JFrame.NORMAL );
                  miframe.repaint();                      
              }             
              

              public void mouseEntered(MouseEvent evt) {}

              public void mouseExited(MouseEvent evt) {}

              public void mousePressed(MouseEvent evt) {}

              public void mouseReleased(MouseEvent evt) {}
          };

          //ACCIONES DEL MENU POPUP
          //Salir de aplicacion
          ActionListener exitListener = new ActionListener() {
              public void actionPerformed(ActionEvent e) {           
                  
                int seleccion = JOptionPane.showOptionDialog(
                miframe,
                "Si cierra el programa no podr� utilizar los drivers de impresi�n del Sistema Nuebus. �Desea Cerrarlo?",                        
                "Confirmaci�n",                 
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,    
                null,   
                JOptionPane.NO_OPTION);          
                  
                
                if (JOptionPane.YES_OPTION == seleccion){                  
                    System.exit(0);
                }else{
                    miframe.setVisible(false);
                }
                    
               
              }
          };
          //Restaurar aplicacion
          ActionListener RestaurarListener = new ActionListener() {
              public void actionPerformed(ActionEvent e) {
                  miframe.setVisible(true);
                  miframe.setExtendedState( JFrame.NORMAL );
                  miframe.repaint();

              }
          };
          //Se crean los Items del menu PopUp y se añaden
          MenuItem SalirItem = new MenuItem("Cerrar");
          SalirItem.addActionListener(exitListener);
          popup.add(SalirItem);

         
          trayIcon.setImageAutoSize(true);
          trayIcon.addMouseListener(mouseListener);


          try {
              systemtray.add(trayIcon);
          } catch (AWTException e) {
              Logger.getLogger().error("AWTException Icon" + e.getMessage() );             
          }
  } else {
        
        JOptionPane.showMessageDialog(null,"Error: SystemTray no es soportado",
                                            "Error de Compatibilidad",JOptionPane.ERROR_MESSAGE);           
         
        LoggerFisico.getInstance().logError("Error: SystemTray no es soportado");
  }

    //Cuando se minimiza JFrame, se oculta para que no aparesca en la barra de tareas
     miframe.addWindowListener(new WindowAdapter(){
        @Override
        public void windowIconified(WindowEvent e){
           miframe.setVisible(false);//Se oculta JFrame
           //Se inicia una tarea cuando se minimiza          
          
        }
    });

   }

    //Muestra una burbuja con la accion que se realiza
    public void MensajeTrayIcon(String texto, MessageType tipo){
        trayIcon.displayMessage("Servidor de Impresi�n Nuebus:", texto, tipo);
    }
    
}

