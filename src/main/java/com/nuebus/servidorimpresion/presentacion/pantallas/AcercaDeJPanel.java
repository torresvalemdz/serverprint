package com.nuebus.servidorimpresion.presentacion.pantallas;


import com.nuebus.servidorimpresion.versionado.VersionProducto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Valeria
 */
public class AcercaDeJPanel extends JPanel implements ActionListener{
    
    public JFrame padre;

    public AcercaDeJPanel( JFrame padre) {        
        super( false );
        this.padre = padre;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        crearComponentes();
    }
    
    private void crearComponentes(){
        
        JLabel espacio = new JLabel("   ");
        espacio.setHorizontalAlignment(JLabel.LEFT);
        add(espacio);       
        
        JLabel etiquetaUno = new JLabel("Version Producto: " + VersionProducto.getVersion() );
        etiquetaUno.setHorizontalAlignment(JLabel.LEFT);
        add(etiquetaUno);
        
        JLabel etiquetaDos = new JLabel("Version Java Requerida: " +  VersionProducto.getVersionJava());
        etiquetaDos.setHorizontalAlignment(JLabel.LEFT);        
        add(etiquetaDos);        
        
        JLabel etiquetaTres = new JLabel("� " + VersionProducto.getAnioCopyRigth() + " Imaquio S.A.Todos los Derechos Reservados");
        etiquetaTres.setHorizontalAlignment(JLabel.LEFT);        
        add(etiquetaTres);   
        
        JButton btn = new JButton( "Logs Imp..." );
        btn.addActionListener( this );
    
        add( btn );
    }   

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton btn = (JButton)e.getSource();
        if( btn.getText().equals( "Logs Imp..." ) ){    
           new VisorLogsJDialog( padre, "Visor Logs Servidor Impresion"  );
        }
    }
    
}
