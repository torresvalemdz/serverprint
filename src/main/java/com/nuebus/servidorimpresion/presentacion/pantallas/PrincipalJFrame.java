
package com.nuebus.servidorimpresion.presentacion.pantallas;

import com.nuebus.servidorimpresion.utiles.Utiles;
import com.nuebus.servidorimpresion.versionado.VersionProducto;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

/**
 *
 * @author Valeria
 */
public class PrincipalJFrame extends JFrame   implements ActionListener{

    public PrincipalJFrame() throws HeadlessException, IOException {        
        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();        
        setResizable(false);    
        setIconImage( Utiles.getImageIcono() );                  
        setMaximizedBounds( new Rectangle(460, 350) );
        setPreferredSize( new Dimension( 460, 350) );
        setLocationRelativeTo(null);             
        
        crearComponentes();
        
        Icon icon = new Icon( this );
        
        actionCerrarApp();
    } 
    
    private void crearComponentes(){
        
       setTitle("Servidor de Impresión Nuebus " + VersionProducto.getVersion() );       
       getContentPane().setLayout(new BorderLayout());       

       JTabbedPane tabbedPane = new JTabbedPane();     
       tabbedPane.addTab("   Consola   ", new VisorJPanel() );
       tabbedPane.addTab("   Acerca De    ", new AcercaDeJPanel( this ) );       

       getContentPane().add(tabbedPane);      
    }

    private void actionCerrarApp(){
        
       setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);// <- prevent closing
       addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                setExtendedState(JFrame.ICONIFIED);
            }
       });      
    }
    
    public void showServidorImpresion(){
       setVisible(false);
       pack();    
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }   
    
    
}
