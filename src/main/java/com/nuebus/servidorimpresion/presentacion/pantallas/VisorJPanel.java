
package com.nuebus.servidorimpresion.presentacion.pantallas;

import com.nuebus.servidorimpresion.logs.Logger;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

/**
 *
 * @author Valeria
 */
public class VisorJPanel extends JPanel{

    public VisorJPanel() {        
        super( false );        
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBackground(Color.white); 
        crearComponentes();
    }
    
    private void crearComponentes(){        
        JScrollPane visor = new JScrollPane();       
        JTextPane areaVisor = Logger.getLogger().getAreaVisor();
        areaVisor.setPreferredSize(new Dimension(460,350));
        areaVisor.setMinimumSize(new Dimension(460,350));
        visor.setViewportView(areaVisor);        
        visor.setBounds(0, 0, 460, 350);
        visor.setPreferredSize(new Dimension(460,350));
        visor.setMinimumSize(new Dimension(460,350));
        add(visor);        
    }
    
}
