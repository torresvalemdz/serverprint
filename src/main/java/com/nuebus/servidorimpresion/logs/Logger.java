
package com.nuebus.servidorimpresion.logs;

import com.nuebus.servidorimpresion.presentacion.server.ServidorWeb;
import com.nuebus.servidorimpresion.utiles.Utiles;
import java.awt.Color;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 *
 * @author Valeria
 */
public class Logger {
    private static Logger log = null;
    private  JTextPane areaVisor = null;
    private  final int MAX_LINEAS = 15;
    private  int cantLineas = 0;
    private  int longitudCorte= 1;

    private Logger(){
        areaVisor = new JTextPane();        
    }

    static public synchronized Logger getLogger(){
        if( log == null){
            log = new Logger();
        }
        return log;
    }
    
    public void config( String msg){
        config( msg , true );
    }

    public void config( String msg, boolean escribeFisico ){
        SimpleAttributeSet attrs = new SimpleAttributeSet();
        StyleConstants.setForeground(attrs, Color.BLUE);
        escribirLog( "CONF: "+ Utiles.parseDateToString(new java.util.Date() , Utiles.FORMAT_DATEHOUR ) + " " +  msg + "\n", attrs);    
        if( escribeFisico ){ LoggerFisico.getInstance().logInfo(msg); }        
    }
    
    public void error( String msg ){
        error( msg, true );
    }
    
    public void error( String msg, boolean escribeFisico ){
        SimpleAttributeSet attrs = new SimpleAttributeSet();
        StyleConstants.setForeground(attrs, Color.red);
        escribirLog( "ERROR: "+ Utiles.parseDateToString(new java.util.Date() , Utiles.FORMAT_DATEHOUR ) + " " +  msg + "\n", attrs);    
        if( escribeFisico ){ LoggerFisico.getInstance().logError(msg); }        
    }

    public void info( String msg){
        info( msg, true );
    }
    
    public void info( String msg, boolean escribeFisico ){        
        SimpleAttributeSet attrs = new SimpleAttributeSet();
        StyleConstants.setBold(attrs, true);        
        escribirLog("INFO: " + Utiles.parseDateToString(new java.util.Date() , Utiles.FORMAT_DATEHOUR ) + " " + msg + "\n" , attrs);
        if( escribeFisico ){ LoggerFisico.getInstance().logInfo(msg); }        
    }

    private  synchronized void  escribirLog( String msg, SimpleAttributeSet attrs){        
        if( areaVisor != null && msg != null ){
            try {               
                areaVisor.getStyledDocument().insertString( ServidorWeb.MJE_ARRANQUE_OK.length(), msg , attrs);          
                cantLineas++;                 
                if( cantLineas == MAX_LINEAS ) {                    
                   
                    if( longitudCorte > 0  ){                   
                        areaVisor.getStyledDocument().remove( ( areaVisor.getStyledDocument().getLength() - longitudCorte )
                                , longitudCorte );              
                    }                    
                    longitudCorte = areaVisor.getStyledDocument().getLength();                                      
                    cantLineas = 0;                   
                    
                }                
                
            } catch (BadLocationException ex) {                
                LoggerFisico.getInstance().logError("BadLocationException ", ex);
            }
        }       
    }
    
    public synchronized void  loggearArranqueOK( String msg ){ 
        
        SimpleAttributeSet attrs = new SimpleAttributeSet();
        StyleConstants.setForeground(attrs, Color.BLUE);        
        LoggerFisico.getInstance().logInfo(msg); 
        
        if( areaVisor != null && msg != null ){
            try {               
                areaVisor.getStyledDocument().insertString( 0, msg , attrs);                         
            } catch (BadLocationException ex) {                
                LoggerFisico.getInstance().logError("BadLocationException ", ex);
            }
        }       
    }
    
    public JTextPane getAreaVisor(){
        return areaVisor;
    }
}
