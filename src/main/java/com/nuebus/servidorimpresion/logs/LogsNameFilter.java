package com.nuebus.servidorimpresion.logs;

import java.io.File;
import java.io.FilenameFilter;


/**
 *
 * @author Valeria
 */
public class LogsNameFilter implements FilenameFilter{

    @Override
    public boolean accept( File dir, String name ) {
        System.out.println("name: " + name  + name.startsWith( LoggerFisico.NAME_LOG ));
        return name.startsWith( LoggerFisico.NAME_LOG );
    }

 
   
 
   
}
