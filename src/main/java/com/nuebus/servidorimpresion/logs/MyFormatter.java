
package com.nuebus.servidorimpresion.logs;

/**
 *
 * @author Valeria
 */
import com.nuebus.servidorimpresion.utiles.Utiles;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
 
public class MyFormatter extends Formatter {
 
    @Override
    public String format(LogRecord record) {
        
        StringWriter errors = new StringWriter();
        
        if(  record.getThrown() != null){
            record.getThrown().printStackTrace(new PrintWriter(errors)); 
        }        
        
        return   Utiles.parseDateToString(new java.util.Date(record.getMillis()) , Utiles.FORMAT_DATEHOUR )+" "
                + record.getLevel()+ " "                            
                + record.getMessage()+ " "
                + errors.toString()
                +"\n";
    } 
}