
package com.nuebus.servidorimpresion.logs;
/**
 *
 * @author Valeria
 */
import com.nuebus.servidorimpresion.utiles.Utiles;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class LoggerFisico {
    
    static public String NAME_LOG = "servImpresion";
    
    static LoggerFisico loggerFisico = null;    
    static final Logger logger = Logger.getLogger(LoggerFisico.class.getName());
    
    public LoggerFisico(){        
        try {            
             
            /*FileInputStream propStream = new FileInputStream(Utiles.getFilePathLogProperties());           
            LogManager.getLogManager().readConfiguration( propStream );      */
            
            
            ClassLoader classLoader = ClassLoader.getSystemClassLoader();
            InputStream inputStream = classLoader.getResourceAsStream("log.properties");
            LogManager.getLogManager().readConfiguration( inputStream );
            
            Handler fileHandler = new FileHandler(Utiles.getUbicacionImpOUT(NAME_LOG), 1000000, 5);
            fileHandler.setFormatter(new MyFormatter());
            fileHandler.setLevel(Level.ALL);
            logger.setLevel(Level.ALL);
            logger.addHandler(fileHandler);
            logger.setUseParentHandlers(false);             
            
        }catch(IOException ex ){
            ex.printStackTrace();
        } catch (SecurityException e1) {
            com.nuebus.servidorimpresion.logs.Logger.getLogger().error( "Logger Fisico " + e1.getMessage() );
        }         
    }
    
    public static synchronized LoggerFisico getInstance(){
        if( loggerFisico == null ){
            loggerFisico = new LoggerFisico();            
        } 
        return loggerFisico;
    }
    
    public void logError( String mensaje ){        
        logger.log(Level.SEVERE, mensaje);        
    }
    public void logInfo( String mensaje ){
        logger.log(Level.INFO, mensaje);        
    }            
    public void logWarning( String mensaje ){
        logger.log(Level.WARNING, mensaje);        
    }    
    
    public void logError( String mensaje, Exception ex ){
        logger.log(Level.SEVERE, mensaje, ex);        
    } 
   
}
