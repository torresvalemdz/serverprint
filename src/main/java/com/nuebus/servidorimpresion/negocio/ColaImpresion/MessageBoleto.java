
package com.nuebus.servidorimpresion.negocio.ColaImpresion;

import com.nuebus.servidorimpresion.negocio.impresion.PageCanva;
import java.util.ArrayList;
import com.nuebus.servidorimpresion.entities.ImpresoraFisica;
import java.util.List;
/**
 *
 * @author  Valeria
 */
public class MessageBoleto {
    
    List<PageCanva> pages = new ArrayList<>();       
    ImpresoraFisica impFisica = new ImpresoraFisica();       

    public MessageBoleto(  List<PageCanva> pages, ImpresoraFisica impFisica ) {
        this.pages = pages;
        this.impFisica = impFisica;
    }   
    
    
    public boolean validateImpresora(){           
        boolean retorno = true;        
        if( ( impFisica == null ) 
                ||( impFisica != null && !impFisica.validate() ) ){
            retorno = false;
        }        
        return retorno;    
    }
   
    public boolean validateBoletos(){           
        boolean retorno = true;        
        if( pages == null 
                || (  pages != null && pages.isEmpty() ) ){
            retorno = false;
        }        
        return retorno;    
    }
    
    public List<PageCanva> getPaginas(){
        return pages;
    }
    
    public ImpresoraFisica getImpresora(){
        return impFisica;    
    }

    public void setPages(List<PageCanva> pages) {
        this.pages = pages;
    }

    public void setImpFisica(ImpresoraFisica impFisica) {
        this.impFisica = impFisica;
    }   
    
}    
