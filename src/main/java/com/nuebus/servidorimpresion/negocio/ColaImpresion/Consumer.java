
package com.nuebus.servidorimpresion.negocio.ColaImpresion;

/**
 *
 * @author Valeria
 */
import com.nuebus.servidorimpresion.logs.Logger;
import com.nuebus.servidorimpresion.negocio.impresion.Printer;
import com.nuebus.servidorimpresion.utiles.UsuarioException;
import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable{

private BlockingQueue<MessageBoleto> queue;

    public Consumer(BlockingQueue<MessageBoleto> q){
        this.queue=q;
    }

    @Override
    public void run() {
        try{
            MessageBoleto msg;
            //consuming messages until exit message is received
            while( true ){
                msg = queue.take();                
               if(  ( msg.getImpresora() != null )  && ( msg.getPaginas() != null )  ){          
                    Printer printer = new Printer(msg.getImpresora(), msg.getPaginas());
                    printer.print();            
               }             
            }
        }catch(InterruptedException e) {
            Logger.getLogger().error( "InterruptedException Consumer " + e.getMessage() ); 
        } catch( UsuarioException ex ){            
            //Logger.error( "Error Consumer " + ex.getMessage() ); 
            /*ya la excepcion tiene el logueo del error*/
        } 
    }
}