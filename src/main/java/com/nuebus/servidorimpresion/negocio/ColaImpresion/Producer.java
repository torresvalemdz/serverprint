package com.nuebus.servidorimpresion.negocio.ColaImpresion;

/**
 *
 * @author Valeria
 */
import com.nuebus.servidorimpresion.logs.Logger;
import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {    

    MessageBoleto msg;
    private BlockingQueue<MessageBoleto> queue;

    public Producer(BlockingQueue<MessageBoleto> q, MessageBoleto m){
        this.queue=q;
        this.msg=m;
    }
    @Override
    public void run() {
        //produce messages        
        try {            
            queue.put(msg);            
        } catch (InterruptedException e) {
            Logger.getLogger().error("InterruptedException Producer " + e.getMessage() );
        }
    }

}