

package com.nuebus.servidorimpresion.negocio.ColaImpresion;

/**
 *
 * @author Valeria
 */
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


public class ProducerConsumerService {
    BlockingQueue<MessageBoleto> queue;
    static ProducerConsumerService impresionService = null;    

    public ProducerConsumerService(){
        queue = new ArrayBlockingQueue<MessageBoleto>(50);
        Consumer consumer = new Consumer(queue);
        new Thread(consumer).start();
    }   

    public synchronized void produce( MessageBoleto mensaje ){
       
         Producer producer = new Producer(queue, mensaje );
         new Thread(producer).start();
    }

    public static ProducerConsumerService getInstance(){
        if( impresionService == null ){
           impresionService = new ProducerConsumerService();
        }       

        return impresionService;
    }

}