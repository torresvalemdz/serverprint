

package com.nuebus.servidorimpresion.negocio.impresion;


import com.nuebus.servidorimpresion.presentacion.BuilderElemento.Elemento;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MediaTracker;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.net.URL;
import java.util.ArrayList;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Chromaticity;
import javax.print.attribute.standard.PrinterName;
import javax.swing.JOptionPane;
import com.nuebus.servidorimpresion.entities.ImpresoraFisica;
import com.nuebus.servidorimpresion.logs.Logger;
import com.nuebus.servidorimpresion.utiles.UsuarioException;
import com.nuebus.servidorimpresion.utiles.Utiles;
import java.util.List;



public class Printer extends Component implements Printable{    
    List<PageCanva> paginas = new ArrayList();    
    ImpresoraFisica impFisica = null;
    static final int NEGRITA = 1;
    URL urlBase= null;
    
    
    public Printer(ImpresoraFisica impFisica, List<PageCanva> paginas){
        super();               
        this.paginas = paginas;        
        this.impFisica = impFisica;        
    }
    
    public void print(){       

        final double PULGADA = 2.8346456692913385826771653543307;
        //The ISO A4 size: 210 mm by 297 mm. 
        PrintRequestAttributeSet aset = new  HashPrintRequestAttributeSet();                   
        //aset.add(MediaSizeName.ISO_A4);
        aset.add(Chromaticity.MONOCHROME);    
        AttributeSet impresora = new HashAttributeSet();        
        impresora.add(new PrinterName(impFisica.getImpresora(), null));

        /* Create a print job */
        PrinterJob pj = PrinterJob.getPrinterJob();
        pj.setJobName("Boletos Nuebus");
        
        PageFormat pf = pj.defaultPage();
        Paper paper = new Paper();
        //MediaPrintableArea mpa = null;
        if(impFisica.getOrientacion() == ImpresoraFisica.HORIZONTAL){
          //aset.add(OrientationRequested.LANDSCAPE);
          //mpa =  new MediaPrintableArea( 0,0,impFisica.getAltoPagina(),290,MediaPrintableArea.MM);
          pf.setOrientation(PageFormat.LANDSCAPE);         
        }else{
          //aset.add(OrientationRequested.PORTRAIT);
          //mpa =  new MediaPrintableArea( 0,0,210,impFisica.getAltoPagina(),MediaPrintableArea.MM);
          pf.setOrientation(PageFormat.PORTRAIT);          
        }
        //aset.add(mpa);
        paper.setSize( 210 * PULGADA ,impFisica.getAltoPagina() * PULGADA );
        paper.setImageableArea( 0, 0, 210 * PULGADA ,impFisica.getAltoPagina() * PULGADA );
        pf.setPaper(paper);   
        pj.setPrintable(this,pf);             
        //aset.add(ColorSupported.SUPPORTED); // si quisieras buscar ademas las que soporten color
        try {
            PrintService[] services  = PrintServiceLookup.lookupPrintServices( null, impresora);
            if( (services !=  null) && (services.length > 0) ){
                pj.setPrintService(services[0]);
                pj.print(aset);
            }else{              
               JOptionPane.showMessageDialog(null,"Debe configurar la impresora en la opción de menu: \n" +
                                            "Encargado de Agencia/Procesos Especiales/Panel de Control de Impresoras.",
                                            "Error de Configuración",JOptionPane.ERROR_MESSAGE);        

            }  
        } catch (PrinterException e) {
            throw  new UsuarioException("PrinterException " +  e.getMessage());
        } catch( Exception ex ){                
            throw  new UsuarioException("Error armado Impresion " + Utiles.getErrorStack(ex) );                 
        }
     
    }

   
    public int print(Graphics g, PageFormat pf, int pagina){
        if(pagina < paginas.size()){            

            MediaTracker mt = new MediaTracker(this);
            //72 coordenadas = 1'' --- 1'' = 25.4 mm --  1 mm = 0,0393700787402''
            // 0,0393700787402'' * 72 = 2,8346456692913385826771653543307 
            final double PULGADA = 2.8346456692913385826771653543307;
            Graphics2D g2d = (Graphics2D)g;
            //Dejo los margenes minimos especificados            

            g2d.translate( pf.getImageableX() + (impFisica.getMargenIzquierdo() * PULGADA), pf.getImageableY() + (impFisica.getMargenSuperior()* PULGADA) );
            
            Font normal = null;
            Font negrita = null;                                 
            PageCanva unaPagina = paginas.get(pagina);
            
            for(  Elemento elemento: unaPagina.getElementos() ){                
             
                normal = new Font(impFisica.getTipoDeLetra(),Font.PLAIN,impFisica.getTamanio() + elemento.getSizeLetra());
                negrita = new Font(impFisica.getTipoDeLetra(),Font.BOLD,impFisica.getTamanio() + elemento.getSizeLetra());
                
                elemento.setNormal(normal);
                elemento.setNegrita(negrita);                   
                elemento.dibujar(g2d, mt, this );
            }           
            
            return (PAGE_EXISTS);
        }else{
            return NO_SUCH_PAGE;
        }     
   }    
}