/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.nuebus.servidorimpresion.negocio.impresion;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author Del Valle
 */
public class BarCodeEAN13 extends BarCode {

  protected static String izqA[]={"0001101","0011001","0010011","0111101","0100011","0110001","0101111","0111011","0110111","0001011"};
  protected static String izqB[]={"0100111","0110011","0011011","0100001","0011101","0111001","0000101","0010001","0001001","0010111"};
  protected String dcha[]={"1110010","1100110","1101100","1000010","1011100","1001110","1010000","1000100","1001000","1110100"};
  protected String CodificaIzq[]={"AAAAA","ABABB","ABBAB","ABBBA","BAABB","BBAAB","BBBAA","BABAB","BABBA","BBABA"};
  protected String inicioFinCode = "101";
  protected String separacionCode = "01010";

  public BarCodeEAN13(){
      setTipo( EAN13);
      setWeigth(100);
      setHeigth(25);
      setWeigthBarPixels(1);
      setHeigthBarPixels(25);
  }

  private String encoderBits(String numero){

        String encoder = CodificaIzq[Integer.parseInt(numero.substring(0,1))];
        //siempre con la izqA el subindice 1
        String codigoBarra = inicioFinCode;
        
        //El segundo digito siempre va con la parte izquierda/////////
        codigoBarra += izqA[Integer.parseInt(numero.substring(1,2))];

        for(int i =0; i < encoder.length(); i++ ){
            codigoBarra += getIzquierda(encoder.substring(i,i+1))[Integer.parseInt(numero.substring(i+2,i+3))];
        }        
       codigoBarra += separacionCode;
       //Parte derecha
       for(int i= 7; i < numero.length(); i++){
           codigoBarra += dcha[Integer.parseInt(numero.substring(i,i+1))];
       }
       codigoBarra += inicioFinCode;
       
       return codigoBarra;
   }

  private String [] getIzquierda(String letra){
       if(letra.equals("A")){
           return izqA;
       }else{
           return izqB;
       }
  }

   private String getCodeVerificador(String code){
       int valor = 0;
       int sumatoria = 0;

       for(int i = code.length()-1 ; i >= 0 ; i--){
           valor = Integer.parseInt(code.substring(i,i + 1));

           if(i % 2 == 0){
               sumatoria +=  valor;
           }else{
               sumatoria += (valor * 3 );
           }
       }
       int codigo = (sumatoria % 10);

       if(codigo != 0 ){
           codigo = 10 - codigo;
       }       
       return String.valueOf(codigo);
    }

    public  BufferedImage getBarcode(String data){
       BufferedImage thumb = new BufferedImage(getWeigth(),getHeigth(),BufferedImage.TYPE_4BYTE_ABGR );
       Graphics2D grafico =  thumb.createGraphics();
       String codigo = encoderBits(data + getCodeVerificador(data));

       int x =0;
       for(int i=0; i < codigo.length();i++ ){
          if(codigo.substring(i,i+1).equals("1")){
             grafico.setColor(Color.black);
             grafico.fillRect(x,0, getWeigthBarPixels(),getHeigthBarPixels());
           }
          x+=1;
      }
      grafico.dispose();

      return thumb;
   }

}
