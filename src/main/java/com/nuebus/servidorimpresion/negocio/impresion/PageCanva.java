
package com.nuebus.servidorimpresion.negocio.impresion;

import com.nuebus.servidorimpresion.presentacion.BuilderElemento.Elemento;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Valeria
 */
public class PageCanva {
    List<Elemento> elementos = new ArrayList<>();

    public PageCanva( List<Elemento> elementos ) {
        this.elementos = elementos;
    }   
   
    public void addElemento( Elemento elemento ){
        elementos.add( elemento );
    }

    public List<Elemento> getElementos() {
        return elementos;
    }  

    @Override
    public String toString() {
        return "PaginaDibujo{" + "elementos=" + elementos + '}';
    }
    
    
}
