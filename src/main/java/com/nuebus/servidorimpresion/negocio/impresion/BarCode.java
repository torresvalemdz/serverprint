
package com.nuebus.servidorimpresion.negocio.impresion;

import java.awt.image.BufferedImage;

public abstract class BarCode {
    public static final int EAN13 = 1;
    public static final int PDF417 = 2;
    protected int weigth = 0;
    protected int heigth = 0;
    protected int weigthBarPixels = 0;
    protected int heigthBarPixels = 0;
    protected int tipo = EAN13;    

    public BarCode(){

    }

    public int getHeigth() {
        return heigth;
    }

    public void setHeigth(int heigth) {
        this.heigth = heigth;
    }

    public int getHeigthBarPixels() {
        return heigthBarPixels;
    }

    public void setHeigthBarPixels(int heigthBarPixels) {
        this.heigthBarPixels = heigthBarPixels;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getWeigth() {
        return weigth;
    }

    public void setWeigth(int weigth) {
        this.weigth = weigth;
    }

    public int getWeigthBarPixels() {
        return weigthBarPixels;
    }

    public void setWeigthBarPixels(int weigthBarPixels) {
        this.weigthBarPixels = weigthBarPixels;
    }
    
    public abstract BufferedImage getBarcode (String dataToEncode );

}
