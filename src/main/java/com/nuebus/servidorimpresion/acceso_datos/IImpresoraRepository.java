
package com.nuebus.servidorimpresion.acceso_datos;

import com.nuebus.servidorimpresion.entities.ImpresoraFisica;

/**
 *
 * @author Valeria
 */
public interface IImpresoraRepository {
    
    public boolean save(String nombreImpresora, ImpresoraFisica impresora);
    public ImpresoraFisica search( String idImpresora );
    public ImpresoraFisica searchSinCache(String nombreImpresora);
    
}
