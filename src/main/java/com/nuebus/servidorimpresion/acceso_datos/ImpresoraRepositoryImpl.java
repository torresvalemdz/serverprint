package com.nuebus.servidorimpresion.acceso_datos;

import com.nuebus.servidorimpresion.entities.ImpresoraFisica;
import com.nuebus.servidorimpresion.utiles.UsuarioException;
import com.nuebus.servidorimpresion.utiles.Utiles;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Properties;

/**
 *
 * @author Valeria
 */
public class ImpresoraRepositoryImpl implements IImpresoraRepository{
    
    static File  directorioImp = null;    
    
    @Override
    public ImpresoraFisica search( String idImpresora ) {
        return ImpresorasCache.getInstance().getImpresora( idImpresora );
    }
    
    @Override
    public  ImpresoraFisica searchSinCache(String nombreImpresora){
        ImpresoraFisica impresora = null;     
        ObjectInputStream entrada;
        try {
            
            File fileEntrada = new File(getUbicacionImpIN(nombreImpresora));
            if(fileEntrada.exists()){                
                entrada = new ObjectInputStream(new FileInputStream(fileEntrada));
                Properties imp = (Properties)entrada.readObject();
                entrada.close();
                if(imp != null){
                    impresora = new ImpresoraFisica();  
                    impresora.setImpresora(imp.getProperty("impresora",""));
                    impresora.setTipoDeLetra(imp.getProperty("tipoDeLetra",""));
                    impresora.setTamanio(Utiles.parseInt(imp.getProperty("tamanio","0")));
                    impresora.setMargenIzquierdo(Utiles.parseInt(imp.getProperty("margenIzquierdo","0")));
                    impresora.setMargenSuperior(Utiles.parseInt(imp.getProperty("margenSuperior","0")));
                    impresora.setOrientacion(Utiles.parseInt(imp.getProperty("orientacion","0")));
                }
            }
            
        } catch (FileNotFoundException ex) {           
           throw  new UsuarioException("FileNotFoundException " + ex.getMessage() );
        }catch (ClassNotFoundException ex) {           
           throw  new UsuarioException( "ClassNotFoundException " + ex.getMessage() );           
        }catch (IOException ex) {           
           throw  new UsuarioException( "IOException " + ex.getMessage() );          
        }
        return impresora;
    }
    
    @Override
    public boolean save(String nombreImpresora, ImpresoraFisica impresora) {       
        
        boolean todoOK = false;        
        ObjectOutputStream salida;
        
        Properties imp = new Properties();
        imp.setProperty("impresora",impresora.getImpresora());
        imp.setProperty("tipoDeLetra",impresora.getTipoDeLetra());
        imp.setProperty("tamanio",String.valueOf(impresora.getTamanio()));
        imp.setProperty("margenIzquierdo",String.valueOf( impresora.getMargenIzquierdo()));
        imp.setProperty("margenSuperior",String.valueOf( impresora.getMargenSuperior()));
        imp.setProperty("orientacion",String.valueOf(impresora.getOrientacion()));

        try {
             salida = new ObjectOutputStream(new FileOutputStream(getUbicacionImpOUT(nombreImpresora)));
             salida.writeObject(imp);
             salida.flush();
             salida.close();
             todoOK = true;
             ImpresorasCache.getInstance().setImpresora( nombreImpresora , impresora );
        } catch (FileNotFoundException ex) {                
            todoOK = false;                            
            throw  new UsuarioException( "FileNotFoundException" + ex.getMessage() ); 
        }catch (IOException ex) {                
            todoOK = false;            
            throw  new UsuarioException( "IOException" + ex.getMessage() ); 
        }
       
        return todoOK; 
        
    }
    
    public static String getUbicacionImpIN(String nombreImpresora){        
        return getDirImpresora().getPath() + File.separator + nombreImpresora + ".ini";
    }
    public static String getUbicacionImpOUT(String nombreImpresora){
        if(!getDirImpresora().exists()){
            getDirImpresora().mkdir();
        }       

        return getDirImpresora().getPath() + File.separator + nombreImpresora + ".ini";
    }

    public static File getDirImpresora(){
        if(directorioImp == null){            
           directorioImp = new File(System.getProperty("user.home"));
           directorioImp = new File(directorioImp,"Impresoras Nuebus");                  
        }

        return directorioImp;
    }
    
}
