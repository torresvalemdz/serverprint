
package com.nuebus.servidorimpresion.acceso_datos;

import com.nuebus.servidorimpresion.entities.ImpresoraFisica;
import com.nuebus.servidorimpresion.presentacion.services.IImpresoraService;
import com.nuebus.servidorimpresion.presentacion.services.impl.ImpresoraServiceImpl;
import java.util.Properties;

/**
 *
 * @author Valeria
 */
public class ImpresorasCache {
    
    IImpresoraService impresoraService = new ImpresoraServiceImpl();

    static  ImpresorasCache impresorasCache = null;
    private Properties impresoras  = new Properties();
    static public ImpresorasCache getInstance(){
        if( impresorasCache == null ){
            impresorasCache = new ImpresorasCache();
        }
        return impresorasCache;
    }

    public void setImpresora(String nombreImpresora, ImpresoraFisica impresora ){
        impresoras.put(nombreImpresora, impresora);
    }

    public ImpresoraFisica getImpresora( String nombreImpresora ){
        ImpresoraFisica imp = null;
        if( nombreImpresora != null  && nombreImpresora.length() > 0 ){
             imp = (ImpresoraFisica)impresoras.get(nombreImpresora);

             if( imp == null ){
                 imp = impresoraService.searchSinCache(nombreImpresora);
                 if( imp != null  ){
                     impresoras.put(nombreImpresora, imp);
                 }
             }
        }
        return  imp;
    }
}
