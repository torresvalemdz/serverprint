package com.nuebus.servidorimpresion.utiles;

import com.nuebus.servidorimpresion.ServImpresionApp;
import com.nuebus.servidorimpresion.logs.Logger;
import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.imageio.ImageIO;


/**
 *
 * @author Del Valle
 */
public class Utiles {
    
    public static String NONBRE_ICONO = "imageServ.png";
    public static String LOG_PROPERTIES =  "log.properties";
    
    public static final double PULGADA  = 2.8346456692913385826771653543307;;
    
    public static final String TOKEN_CAMPO = "&++";
    public static final String TOKEN_LINEA = "&--";
    public static final String TOKEN_PAGINA = "&**";
    

    public static final String STR_HORIZONTAL = "HORIZONTAL";
    public static final String STR_VERTICAL = "VERTICAL";
    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;
    
    public static final String FORMAT_DATEHOUR = "dd/MM/yyyy HH:mm:ss";
    public static final String FORMAT_DATE = "dd/MM/yyyy";
    public static final String FORMAT_YEAR = "yyyy";

    static public final String KEYS_ORIENTACION [] = { String.valueOf(VERTICAL),String.valueOf(HORIZONTAL) };
    static public final String VALUES_ORIENTACION [] = {STR_VERTICAL, STR_HORIZONTAL };

    public static int parseInt(String _value){
        int retorno = 0;
        try{
            retorno = Integer.parseInt(_value.trim());
        } catch (Exception e) {
            retorno = 0;
        }
        return retorno;
    }

      public static double parseDouble(String _value){
        double retorno = 0;
        try{
            retorno = Double.parseDouble(_value.trim());
        } catch (Exception e){
            retorno = 0;
        }
        return retorno;
    }

    public static String parseError( List<String> errores, String separador ) {
        String erroresString= "";
        if(!errores.isEmpty()){             
             erroresString = errores.stream().map((error) -> error+separador)
                     .reduce(erroresString, String::concat);   
             
             if( erroresString.endsWith(separador) ){
                 erroresString = erroresString.substring(0, erroresString.length() -1 );
             }
        } 
        return erroresString;
    }
    
    public static String parseDateToString (java.util.Date date, String format){        
      String fecha = "";
      if( date != null && format != null && format.length() >0 ){
          SimpleDateFormat templateFecha = new SimpleDateFormat( format );      
          fecha =  templateFecha.format(date);       
      }      
      return fecha;                      
    }            
     
    public static String parseDateToString (java.util.Date date){        
      return parseDateToString (date, FORMAT_DATE);        
    } 
    
    
    public static String getUbicacionOUT(String nombreFullArch){
        String archivoOUT = "";
        File directorioConf = getDirConf();
        if(directorioConf!= null ){            
            if(!directorioConf.exists()){
                directorioConf.mkdir();
            }          
            archivoOUT = directorioConf.getPath() + File.separator + nombreFullArch;
        }
        return archivoOUT;
    }
    
     public static File getDirConf(){
        File directorioConf = null; 
        try{        
            CodeSource codeSource = Utiles.class.getProtectionDomain().getCodeSource();
            File jarFile = new File(codeSource.getLocation().toURI().getPath());
            String jarDir = jarFile.getParentFile().getPath();           
            directorioConf = new File(jarDir);
            directorioConf = new File(directorioConf,"Conf");
        }catch(URISyntaxException e){
            Logger.getLogger().error("URISyntaxException directorio Conf " + e.getMessage() );        
        }
        return directorioConf;
    }
     
    public static String getUbicacionIN(String nombreFullArch){
        String archivoIN = "";
        File directorioConf = getDirConf();
        if( directorioConf != null){
            archivoIN = directorioConf.getPath() + File.separator + nombreFullArch;
        }    
        return archivoIN;
    } 
    
    
    public static File getDirImpresora(){        
        File  directorioImp = new File(System.getProperty("user.home"));            
        directorioImp = new File(directorioImp,"Impresoras Nuebus");
        return directorioImp;
    }
    
    
    public static String getUbicacionImpOUT(String nombreLog){
        if(!getDirImpresora().exists()){
            getDirImpresora().mkdir();
        }       
        return getDirImpresora().getPath() + File.separator + nombreLog + ".log";
    }
    
    public static String getErrorStack( Exception ex ){
        String errorStack = "";
        if( ex != null ){
            errorStack+=ex.getMessage();
            StackTraceElement[] excep = ex.getStackTrace();                     
            for(int i=0; i < excep.length; i++ ){
                errorStack+=  "\n" + excep[i].toString();           
            }   
        }        
        return errorStack;
    }

    public static String unZipDatos(byte[] datos){  
        String resultado = "";
        try{        
            InputStream is = new ByteArrayInputStream(datos);                         
            int BUFFER = 100;
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry entry;
            while((entry = zis.getNextEntry()) != null) {
                 int count;
                 byte data[] = new byte[BUFFER];
                 while ( (count = zis.read(data, 0, BUFFER)) != -1) {
                      resultado+=new String(data,0,count);
                 }
            }
            //System.out.println("Resultado " + resultado.length() + " " + resultado);
        }catch(Exception ex){
             Logger.getLogger().error("unZipDatos " + ex.getMessage() );      
        }  
        return resultado;
    }
    
    public static String extraerCampo( String data, int ordenCampo, String tokenCampo ){        
        int hasta=-1;       
        int comienza = 0;   
        String respuesta = "";
        if( data != null && data.length() > 0 
                && tokenCampo != null && tokenCampo.length()> 0 ){
            for( int i=1; i <= ordenCampo; i++ ){            
               if( ( hasta = data.indexOf( tokenCampo, comienza ) ) >= 0 ){
                    respuesta =  data.substring( comienza,  hasta );
                    comienza = hasta +  tokenCampo.length();                        
               }         
            }      
        }        
        return respuesta;
    }
    
    public static List<String> splitByToken( String infoTotal, String token  ){
       List<String> cadenas = new ArrayList<>();       
       int hastaPagina = -1; 
       while( ( hastaPagina = infoTotal.indexOf(token) ) >= 0 ){                      
           cadenas.add( infoTotal.substring(0, hastaPagina) );
           infoTotal =infoTotal.substring(hastaPagina + 3 );
       }           
       return cadenas;
    } 
    
       /**
     * Obtiene la ruta base donde est� corriendo la aplicaci�n, sin importar que sea desde un archivo .class
     * o desde un paquete .jar.
     * */
    public static String getBaseFilePath(){
        String path = ServImpresionApp.class.getProtectionDomain().getCodeSource().getLocation().getPath(); 
        
        System.out.println( " path " + path );
        
        File pathFile = new File(path);
        if(pathFile.isFile()){
           path += File.separator;
        }
        
        System.out.println( " path mod " + path );
        
        return path;
    }
    
    
     public static String getBaseFilePathBK(){
        String path = ServImpresionApp.class.getProtectionDomain().getCodeSource().getLocation().getPath(); 
        
        System.out.println( " path " + path );
        
        File pathFile = new File(path);
        if(pathFile.isFile()){
            path = pathFile.getParent();
            if(!path.endsWith(File.separator)) path += File.separator;

        }
        
        System.out.println( " path mod " + path );
        
        return path;
    }
    
   /* public static String getFilePathIcono(){
        return Utiles.getBaseFilePath() + NONBRE_ICONO;
    }*/
    
    public static String getFilePathLogProperties(){
        return Utiles.getBaseFilePath() + LOG_PROPERTIES;
    }   
    
    public static Image getImageIcono() throws IOException{
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        InputStream is = classLoader.getResourceAsStream( NONBRE_ICONO );
        Image image = ImageIO.read(is);        
        return image;
    }
}
