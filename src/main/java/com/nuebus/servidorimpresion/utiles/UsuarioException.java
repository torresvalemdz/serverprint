
package com.nuebus.servidorimpresion.utiles;

import com.nuebus.servidorimpresion.logs.Logger;

/**
 *
 * @author Valeria
 */
public class UsuarioException extends RuntimeException {
private static final long serialVersionUID = 1L;
public UsuarioException() {
    super();
}
public UsuarioException(String message, Throwable cause) {
    super(message, cause);
    Logger.getLogger().error( message + cause.getMessage());
}

public UsuarioException(String message) {
    super(message);
    Logger.getLogger().error( message );
}
public UsuarioException(Throwable cause) {
    super(cause);
}    

}