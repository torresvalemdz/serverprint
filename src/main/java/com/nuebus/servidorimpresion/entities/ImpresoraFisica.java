package com.nuebus.servidorimpresion.entities;


import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Del Valle
 */
public class ImpresoraFisica {
    public static final String STR_HORIZONTAL = "HORIZONTAL";
    public static final String STR_VERTICAL = "VERTICAL";
    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;

    public static final String KEYS_ORIENTACION [] = { String.valueOf(VERTICAL),String.valueOf(HORIZONTAL) };
    public static final String VALUES_ORIENTACION [] = {STR_VERTICAL, STR_HORIZONTAL };

   
    String impresora = "";
    String tipoDeLetra = "";
    int tamanio = 0;
    int margenIzquierdo = 0;
    int margenSuperior = 0;
    int orientacion = -1;
    int altoPagina= 0;
    
    List<String> errores = new ArrayList();
    
    public void ImpresoraFisica(){        
    }
      

    public boolean validate(){
        errores = new ArrayList();
        
        if( impresora.length()<=0 ){
           errores.add("Debe Seleccionar una impresora.");        
        }
        if( tipoDeLetra.length()<=0 ){
            errores.add("Debe Seleccionar un Tipo de Letra.");
        }
        if( tamanio <= 0){
            errores.add("Debe especificar un Tama&ntilde;o de letra.");            
        }
        if( orientacion <0 ){
            errores.add("Debe especificar la Orientaci&oacute;n de la pagina.");
        }            
        return errores.isEmpty();
    }

    public List<String> validateAndGetErrores(){
        validate();
        return errores;
    }
    
    public String getImpresora() {
        return impresora;
    }

    public void setImpresora(String impresora) {
        if(impresora != null){
            this.impresora = impresora;
        }
    }

    public int getMargenSuperior() {
        return margenSuperior;
    }

    public void setMargenSuperior(int margenSuperior) {
        this.margenSuperior = margenSuperior;
    }

    public int getMargenIzquierdo() {
        return margenIzquierdo;
    }

    public void setMargenIzquierdo(int margenIzquierdo) {
        this.margenIzquierdo = margenIzquierdo;
    }

    public int getOrientacion() {
        return orientacion;
    }

    public void setOrientacion(int orientacion) {
        this.orientacion = orientacion;
    }   
    
    public String getStrOrientacion() {
        String orientacionStr = "";
        switch (orientacion){
            case HORIZONTAL:
                orientacionStr = STR_HORIZONTAL;
                break;
            case VERTICAL:
                orientacionStr = STR_VERTICAL;
                break;
            default:
                orientacionStr = "";
        }
        return orientacionStr;
    }

    public void setStrOrientacion(String orientacion) {
        if(orientacion != null){
            if(orientacion.equals(STR_HORIZONTAL)){
                this.orientacion = HORIZONTAL;
            }else if(orientacion.equals(STR_VERTICAL) ) {
                this.orientacion = VERTICAL;
            }
        }
    }

    public int getTamanio() {
        return tamanio;
    }

    public void setTamanio(int tama�o) {
        this.tamanio = tama�o;
    }

    public String getTipoDeLetra() {
        return tipoDeLetra;
    }

    public void setTipoDeLetra(String tipoDeLetra) {
        if(tipoDeLetra != null){
            this.tipoDeLetra = tipoDeLetra;
        }        
    }
    public void setAltoPagina(int altoPagina){
        this.altoPagina = altoPagina;
    }
    public int getAltoPagina(){
        return altoPagina;
    }   
    public List<String> getErrores(){
        return errores;
    }
       
    @Override
    public String toString() {
        return "ImpresoraFisica{" + "impresora=" + impresora + ", tipoDeLetra=" + tipoDeLetra + ", tamanio=" + tamanio + ", margenIzquierdo=" + margenIzquierdo + ", margenSuperior=" + margenSuperior + ", orientacion=" + orientacion + ", altoPagina=" + altoPagina + ", errores=" + errores + '}';
    }
    
    

}
