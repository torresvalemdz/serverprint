package com.nuebus.servidorimpresion.dtos;

import java.io.Serializable;

/**
 *
 * @author Valeria
 */
public class Impresora implements Serializable {
    
    String  impresora; /* "Microsoft Print to PDF" */
    String tipoDeLetra; /* "Times New Roman" */
    Integer tamanio; /* 8 */
    Integer margenIzquierdo; /* 3 */
    Integer margenSuperior; /* 3 */
    Integer orientacion; /* 1 */

    public String getImpresora() {
        return impresora;
    }

    public void setImpresora(String impresora) {
        this.impresora = impresora;
    }

    public String getTipoDeLetra() {
        return tipoDeLetra;
    }

    public void setTipoDeLetra(String tipoDeLetra) {
        this.tipoDeLetra = tipoDeLetra;
    }

    public Integer getTamanio() {
        return tamanio;
    }

    public void setTamanio(Integer tamanio) {
        this.tamanio = tamanio;
    }

    public Integer getMargenIzquierdo() {
        return margenIzquierdo;
    }

    public void setMargenIzquierdo(Integer margenIzquierdo) {
        this.margenIzquierdo = margenIzquierdo;
    }

    public Integer getMargenSuperior() {
        return margenSuperior;
    }

    public void setMargenSuperior(Integer margenSuperior) {
        this.margenSuperior = margenSuperior;
    }

    public Integer getOrientacion() {
        return orientacion;
    }

    public void setOrientacion(Integer orientacion) {
        this.orientacion = orientacion;
    }   

    @Override
    public String toString() {
        return "Impresora{" + "impresora=" + impresora + ", tipoDeLetra=" + tipoDeLetra + ", tamanio=" + tamanio + ", margenIzquierdo=" + margenIzquierdo + ", margenSuperior=" + margenSuperior + ", orientacion=" + orientacion + '}';
    }   
    
}
