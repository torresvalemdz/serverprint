package com.nuebus.servidorimpresion.dtos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Valeria
 */
public class OpcionesImpresion {
    
    String version = "";
    Impresora impresora;
    Map<String,String> orientacion = new HashMap<>();
    List<String> impresoras = new ArrayList<>();
    List<String> letras = new ArrayList<>();    

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Impresora getImpresora() {
        return impresora;
    }

    public void setImpresora(Impresora impresora) {
        this.impresora = impresora;
    }

    public Map<String, String> getOrientacion() {
        return orientacion;
    }

    public void setOrientacion(Map<String, String> orientacion) {
        this.orientacion = orientacion;
    }

    public List<String> getImpresoras() {
        return impresoras;
    }

    public void setImpresoras(List<String> impresoras) {
        this.impresoras = impresoras;
    }

    public List<String> getLetras() {
        return letras;
    }

    public void setLetras(List<String> letras) {
        this.letras = letras;
    }

    @Override
    public String toString() {
        return "OpcionesImpresion{" + "version=" + version + ", impresora=" + impresora + ", orientacion=" + orientacion + ", impresoras=" + impresoras + ", letras=" + letras + '}';
    }   
    
    
}
