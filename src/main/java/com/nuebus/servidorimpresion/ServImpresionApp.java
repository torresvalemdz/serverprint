package com.nuebus.servidorimpresion;

import com.nuebus.servidorimpresion.presentacion.server.ServidorWeb;
import com.nuebus.servidorimpresion.presentacion.pantallas.PrincipalJFrame;
import com.nuebus.servidorimpresion.utiles.UsuarioException;


/**
 *
 * @author Valeria
 */
public class ServImpresionApp{  

    public static void main(String[] args) throws Exception{
        ServImpresionApp app = new ServImpresionApp();
        app.start();
    }
    
    private void start(){
        
       try{ 
           
            PrincipalJFrame principalJFrame = new PrincipalJFrame();
            principalJFrame.showServidorImpresion();  
           
            ServidorWeb instancia = new ServidorWeb();
            instancia.arranca();
           
                           
                     
       } catch( UsuarioException ex ){
            //LoggerFisico.getInstance().logError("Error App",ex);
       } catch( Exception ex ) {
           ex.printStackTrace();
       }
       
    }    
 
}
